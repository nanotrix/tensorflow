# Start with cuDNN base image
FROM nvidia/cuda:7.5-cudnn5-runtime-ubuntu14.04


MAINTAINER nanotrix <jindrich.zdansky@gmail.com>
ENV DEBIAN_FRONTEND noninteractive
ENV KERAS_BACKEND=tensorflow
ARG MINICONDA_VERSION=4.1.11
ARG TENSORFLOW_VERSION=0.10.0rc0
ARG TENSORFLOW_DEVICE=gpu
ARG KERAS_VERSION=1.0.7
ARG JUPYTERLAB_VERSION=0.2.0

RUN ln -s /usr/lib/x86_64-linux-gnu/libcudnn.so.5 /usr/lib/x86_64-linux-gnu/libcudnn.so
RUN apt-get update \
  && apt-get install -y --no-install-recommends \
#    build-essential \
    curl \
    libgtk2.0 \
    libgomp1 \
#    libglib2.0 \
#    libxext6 \
#    libsm6 \
#    libxrender1 \
#    git \
#    mercurial \
    nano \
#    vim \
  && rm -rf /var/lib/apt/lists/*
  
# Set CUDA_ROOT
ENV CUDA_ROOT /usr/local/cuda/bin

RUN curl -qsSLkO \
	https://repo.continuum.io/miniconda/Miniconda3-${MINICONDA_VERSION}-Linux-`uname -p`.sh \
  && bash Miniconda3-${MINICONDA_VERSION}-Linux-`uname -p`.sh -b \
  && rm Miniconda3-${MINICONDA_VERSION}-Linux-`uname -p`.sh

ENV PATH=/root/miniconda3/bin:$PATH 	

ENV KERAS_BACKEND=tensorflow

RUN conda install -c conda-forge -y \
numpy scipy pandas matplotlib seaborn \
jupyter ipykernel jupyterlab=${JUPYTERLAB_VERSION} \
scikit-learn keras=${KERAS_VERSION} dill scikit-image \
&& conda clean -a

RUN conda install -c menpo -y opencv3 && conda clean -a

RUN pip install --ignore-installed --upgrade https://storage.googleapis.com/tensorflow/linux/${TENSORFLOW_DEVICE}/tensorflow-${TENSORFLOW_VERSION}-cp35-cp35m-linux_x86_64.whl 
  
# Set up .theanorc for CUDA
#RUN echo "[global]\ndevice=gpu\nfloatX=float32\nopenmp = True\n[lib]\ncnmem=1\n[nvcc]\nfastmath=True" > /root/.theanorc

#ENV THEANO_FLAGS="mode=FAST_RUN,device=gpu,floatX=float32"
#ENV OMP_NUM_THREADS=8

# Set up notebook config
COPY jupyter_notebook_config.py /root/.jupyter/
ENV PYTHONPATH=$PYTHONPATH:/workspace

WORKDIR /workspace
