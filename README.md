## Run container with GPU
```bash
nvidia-docker run --rm -it -v host_dir:/workspace -e PASSWORD=xxx -p host_port:8888 nanotrix/tensorflow
```
## Run container with CPU
```bash
docker run --rm -it -v host_dir:/workspace -e PASSWORD=xxx -p host_port:8888 nanotrix/tensorflow
```
## Jupyter notebook

```bash
jupyter notebook
```
## Jupyter lab

```bash
jupyter lab
```

## Build image
```bash
nvidia-docker build -t nanotrix/tensorflow:latest https://bitbucket.org/nanotrix/tensorflow.git
```